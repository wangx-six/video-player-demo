package com.zego.example.videoplayerdemo;

import com.zego.example.videoplayerdemo.slice.MainAbilitySlice;
import ohos.aafwk.ability.Ability;
import ohos.aafwk.content.Intent;
import ohos.bundle.IBundleManager;
import ohos.security.SystemPermission;

public class MainAbility extends Ability {
    private static final int REQUEST_CODE = 0X01;

    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        requestPermission();
        super.setMainRoute(MainAbilitySlice.class.getName());
    }

    private void requestPermission() {
        if (verifyCallingOrSelfPermission(SystemPermission.READ_USER_STORAGE) != IBundleManager.PERMISSION_GRANTED) {
            requestPermissionsFromUser(new String[]{SystemPermission.READ_USER_STORAGE}, REQUEST_CODE);
        }
    }
}
