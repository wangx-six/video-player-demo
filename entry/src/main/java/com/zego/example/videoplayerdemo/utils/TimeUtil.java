package com.zego.example.videoplayerdemo.utils;

public class TimeUtil {
    public static String stringForTime(int timeMillisecond) {
        int timeSeconds = timeMillisecond / 1000;
        int seconds = timeSeconds % 60;
        int minutes = timeSeconds / 60 % 60;
        int hours = timeSeconds / 3600;
        return hours > 0 ? String.format("%d:%02d:%02d", new Object[]{Integer.valueOf(hours), Integer.valueOf(minutes), Integer.valueOf(seconds)}) : String.format("%02d:%02d", new Object[]{Integer.valueOf(minutes), Integer.valueOf(seconds)});
    }
}
